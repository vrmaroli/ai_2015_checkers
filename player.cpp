// Vishnu Ramesh Maroli

#include "player.hpp"
#include <climits>
#include <cstdlib>

namespace checkers {

int score(GameState pState, bool amIRed) {
    int count = 0, avoidDraw = 0;
    if(amIRed) {
        if(pState.isRedWin())
            return INT_MAX;
        else {
            if(pState.getMove().isJump()) {
                avoidDraw+=5;
            }
            for(int i=0; i<32; i++) {
                if(pState.at(i)&CELL_RED) {
                    count+=1;
                    if(pState.at(i)&CELL_KING)
                        count+=2;
                }
                else {
                    count-=2;
                    if(pState.at(i)&CELL_KING)
                        count-=4;

                }
            }
            return count + avoidDraw;
        }
    }
    else {
        if(pState.isWhiteWin())
            return INT_MAX;
        else {
            if(pState.getMove().isJump()) {
                avoidDraw+=5;
            }
            for(int i=0; i<32; i++) {
                if(pState.at(i)&CELL_WHITE) {
                    count+=1;
                    if(pState.at(i)&CELL_KING)
                        count+=2;
                }
                else {
                    count-=2;
                    if(pState.at(i)&CELL_KING)
                        count-=4;

                }
            }
            return count + avoidDraw;
        }
    }
}

int alphabeta(GameState pState, int depth, int alpha, int beta, bool maximizingPlayer, bool amIRed) {
    std::vector<GameState> lNextStates;
    pState.findPossibleMoves(lNextStates);
    if(depth==0 or lNextStates.size() == 0)
        return score(pState, amIRed);
    if(maximizingPlayer) {
        int temp = INT_MIN;
        for(std::vector<GameState>::iterator it=lNextStates.begin(); it<lNextStates.end(); it++) {
            temp = std::max(temp, alphabeta(*it, depth-1, alpha, beta, false, amIRed));
            alpha = std::max(alpha, temp);
            if(beta<=alpha)
                return alpha;
        }
        return temp;
    }
    else {
        int temp = INT_MAX;
        for(std::vector<GameState>::iterator it=lNextStates.begin(); it<lNextStates.end(); it++) {
            temp = std::min(temp, alphabeta(*it, depth-1, alpha, beta, true, amIRed));
            beta = std::min(beta, temp);
            if(beta<=alpha)
                return beta;
        }
        return temp;
    }
}

GameState Player::play(const GameState &pState,const Deadline &pDue) {
    std::vector<GameState> lNextStates;
    pState.findPossibleMoves(lNextStates);

    if (lNextStates.size() == 0) return GameState(pState, Move());
    bool amIRed;
    if(pState.getNextPlayer()&CELL_RED)
        amIRed = true;
    else
        amIRed = false;
    int maxTemp = INT_MIN;
    int depth = 8;
    GameState bestState;
    for(std::vector<GameState>::iterator it=lNextStates.begin(); it<lNextStates.end(); it++) {
        int temp = alphabeta(*it, depth, INT_MIN, INT_MAX, false, amIRed);
        if(temp>=maxTemp) {
            maxTemp = temp;
            bestState = *it;
        }
    }
    return bestState;
}

/*namespace checkers*/ }
